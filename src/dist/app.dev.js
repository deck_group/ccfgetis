"use strict";

var WebSocketServer = require('ws').Server;

var ws = new WebSocketServer({
  port: 7000
});

var redis2 = require('redis').createClient();

var redisSchedulerOver = require('redis').createClient({
  port: 6379,
  host: 'ccfnew-2021010505024215-ms.cn-m3i74bhbbjqbyclj5bceqvvk-ccfnew',
  password: 'macrock1'
});

var redispublish = require('redis').createClient({
  port: 6379,
  host: 'ccfnew-2021010505024215-ms.cn-m3i74bhbbjqbyclj5bceqvvk-ccfnew',
  password: 'macrock1'
});

var redisstoppublish = require('redis').createClient({
  port: 6379,
  host: 'ccfnew-2021010505024215-ms.cn-m3i74bhbbjqbyclj5bceqvvk-ccfnew',
  password: 'macrock1'
});

var redisstoppublishnew = require('redis').createClient({
  port: 6379,
  host: 'ccfnew-2021010505024215-ms.cn-m3i74bhbbjqbyclj5bceqvvk-ccfnew',
  password: 'macrock1'
});

var redisesttime = require('redis').createClient({
  port: 6379,
  host: 'ccfnew-2021010505024215-ms.cn-m3i74bhbbjqbyclj5bceqvvk-ccfnew',
  password: 'macrock1'
});

var client = require('redis').createClient({
  port: 6379,
  host: 'ccfnew-2021010505024215-ms.cn-m3i74bhbbjqbyclj5bceqvvk-ccfnew',
  password: 'macrock1'
});

redis2.subscribe('schedulerlog');
redisSchedulerOver.subscribe('schedulerover');
redisstoppublish.subscribe('setstopjob');
redisstoppublish.subscribe('stopbtntrue');
redisstoppublish.subscribe('clearcomdatarep');
redisstoppublishnew.subscribe('statusnew');
redisesttime.subscribe('esttime');
ws.on('connection', function connection(ws) {
  redis2.on('message', function (channel, data) {
    ws.send(JSON.stringify({
      "data": data,
      "channel": channel
    }));
  });
  redisstoppublish.on('message', function (channel, data) {
    ws.send(JSON.stringify({
      "data": data,
      "channel": channel
    }));
  });
  redisesttime.on('message', function (channel, data) {
    ws.send(JSON.stringify({
      "data": data,
      "channel": channel
    }));
  });
  redisstoppublishnew.on('message', function (channel, data) {
    client.hgetall('statuses', function (err, object) {
      ws.send(JSON.stringify({
        "data": object,
        "channel": channel
      }));
    });
  });
  ws.on('message', function incoming(message) {
    var newmsg = message.split('-');

    if (newmsg[0] == "canceljob") {
      redispublish.publish('stopbtntrue', 'true');
      redispublish.publish('cancelprocess', 'cancel');
    } else if (newmsg[0] == "clearcomdata") {
      redispublish.publish('clearcomdatarep', 'true');
    } else if (newmsg[0] == "jobstatus") {
      client.hset("statuses", newmsg[1], newmsg[2]);
      redispublish.publish('statusnew', 'true');
    } else if (newmsg[0] == "alllog") {
      redispublish.publish('allsub', newmsg[1]);
    } else if (newmsg[0] == "login") {
      redispublish.publish('loginlog', newmsg[1]);
    }
  });
  redisSchedulerOver.on('message', function (channel, data) {
    ws.send(JSON.stringify({
      "data": data,
      "channel": channel
    }));
  });
});