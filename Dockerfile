FROM klovercloud/node:13-slim

ARG NEXUS_NPM_GROUP_URL
ARG NEXUS_TOKEN

RUN useradd -ms /bin/bash klovercloud

WORKDIR /home/klovercloud

COPY package*.json ./

RUN npm install
COPY . .
USER klovercloud
EXPOSE 7000
ENTRYPOINT ["npm", "start"]

